/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employees;

import java.util.ArrayList;

/**
 *
 * @author Satyanarayana Madala
 */

public class Employee implements Comparable<Employee>{
    
    private String empID;
    private String lastName;
    private String firstName;
    private int yearEmployed;
    private ArrayList<String> telephoneNumbers; 
/**
 * Constructor taking the parameters of the employee
 * @param empID Employee unique ID
 * @param lastName LastName of the Employee
 * @param firstName firstName of the Employee
 * @param yearEmployed Year employee got employed
 */
    public Employee(String empID, String lastName, String firstName, int yearEmployed) {
        this.empID = empID;
        this.lastName = lastName;
        this.firstName = firstName;
        this.yearEmployed = yearEmployed;
        this.telephoneNumbers = new ArrayList<>();
        }

    /**
     * gets the EmployeeID
     * @return the ID of the Employee
     */
    public String getEmpID() {
        return empID;
    }
/**
 * gets the lastName of the employee
 * @return The lastName of the employee
 */
    public String getLastName() {
        return lastName;
    }
/**
 * gets the FirstName of the employee
 * @return the FirstName of the employee
 */
    public String getFirstName() {
        return firstName;
    }
/**
 * gets the Year employed
 * @return the yearEmployed
 */
    public int getYearEmployed() {
        return yearEmployed;
    }
/**
 * Adds the number to the list of the particular employee
 * @param telephoneNumber of the employee
 * @throws IncorrectLengthException if the length of the telephone number is incorrect
 * @throws IncorrectAreaCodeException if the area code of the telephone number is incorrect 
 * @throws NonDigitFoundException if the non digit is found in the telephone number
 */
public void addNumber(String telephoneNumber) throws IncorrectLengthException, IncorrectAreaCodeException, NonDigitFoundException{
   
    if(telephoneNumber.length() != 10){
        throw new IncorrectLengthException(telephoneNumber + " IncorrectLengthException: Length of the phone number is not 10");
    }
    else {
        
        String Message = telephoneNumber.replaceAll("[0-9]", "");
        
        if(Message.length() != 0){
                throw new NonDigitFoundException(telephoneNumber + " NonDigitFoundException: Some characters in the phone number are not digits");
          }
        else{
              if(!(telephoneNumber.substring(0, 3).equals("660") ||telephoneNumber.substring(0, 3).equals("816")) ){
            throw new IncorrectAreaCodeException(telephoneNumber + " IncorrectAreaCodeException: Area code must be 660 or 816");
        }
              else{
           telephoneNumbers.add(telephoneNumber);
        }
            
        }
    }
     }
/**
 * gets the output in the specified format
 * @return the employee details in the particular format
 */
    @Override
    public String toString() {
        
        String a = empID +" "+ firstName +" "+lastName +" "+ yearEmployed + "\n";
        String b = "" ;
        String c ="";
        for(int i= 0 ; i< telephoneNumbers.size();i++){
            b = telephoneNumbers.get(i);
         c = b.substring(0, 3)+"-"+b.substring(3,6)+"-"+b.substring(6) + ",";
        a +=c;
            }
        return a.substring(0,(a.length()-1));
    
    }
    
    /**
     * compares the details of the employee 
     * @param o Employee details to compare
     * @return the employee details
     */
    @Override
    public int compareTo(Employee o) {
       if(this.getLastName().compareTo(o.getLastName()) == 0)
		{
		  return this.getFirstName().compareTo(o.getFirstName());
		} else
		{
		  return this.getLastName().compareTo(o.getLastName());
		}
    }
}
