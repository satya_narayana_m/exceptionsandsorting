/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employees;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Satyanarayana Madala
 */
public class EmployeeDriver {
    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("EmployeeData.txt"));
        
        EmployeeList emplist = new EmployeeList();
        Employee employee;
                        while(sc.hasNext()){
                                String empId = sc.next();
                                String lastName =sc.next();
                                String firstName = sc.next();
                                int YearEmployed = sc.nextInt();
                employee = new Employee(empId, lastName, firstName, YearEmployed);   

                                int totalTeleNumbers = sc.nextInt();
                                int count =0 ;
        while (count < totalTeleNumbers){
         String teleNumber = sc.next();
                try{
                    employee.addNumber(teleNumber);
                    }
                catch(IncorrectAreaCodeException | IncorrectLengthException | NonDigitFoundException e){
                    System.out.println( e.getMessage());
                    }
                
               count ++;
    }
        emplist.addEmployee(employee);
      }
        System.out.println();
        System.out.println("ORIGINAL LIST");                        
        for(Employee e : emplist){
        System.out.println(e);
                                }
     
        System.out.println("");
        System.out.println("SORT BY NAME");
        emplist.sortEmployee();
        for(Employee e : emplist){
        System.out.println(e);
                                 }
        
        System.out.println("");
        System.out.println("SORT BY YEAR");
        emplist.sortByYear();
        for(Employee e : emplist){
        System.out.println(e);
                                  }

    }
    
}
