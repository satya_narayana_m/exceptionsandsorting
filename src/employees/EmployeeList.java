/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employees;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

/**
 *
 * @author Satyanarayana Madala
 */
public class EmployeeList implements Iterable<Employee>{
    
    private ArrayList<Employee> employees;
    /**
     * No Argument Constructor initializing the array
     */

    public EmployeeList() {
        
         employees= new ArrayList<Employee>();
    }
    /**
     * Adds the employee to the employeeList
     * @param emp adds employee to the list 
     */
    public void addEmployee(Employee emp){
        employees.add(emp);
    }
    /**
     * iterator to iterate the arrayList in the Driver Class 
     * @return the Iterator Employee
     */

    @Override
    public Iterator<Employee> iterator() {
        return employees.iterator();
     }
    /**
     * sorts the employee in the Given List
     */
 
    public void sortEmployee(){
    Collections.sort(employees);
    }
    /**
     * Sorts the employee by the year they have joined
     */
    
    public void sortByYear(){   
   Collections.sort(employees, new Comparator<Employee>()
{
        @Override
	public int compare(Employee e1, Employee e2)
	{
		if(e1.getYearEmployed()> e2.getYearEmployed())
		{
			return -1;
		} else if(e1.getYearEmployed() == e2.getYearEmployed()){
    
                   return  0;
              }

                    else
		{
			return 1;
		}
	}
});
}
   
    
}
